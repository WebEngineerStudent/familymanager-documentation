-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2019. Jan 28. 09:03
-- Kiszolgáló verziója: 10.1.30-MariaDB
-- PHP verzió: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `familymanager`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `chat`
--

CREATE TABLE `chat` (
  `messageID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `message` text COLLATE utf8_hungarian_ci NOT NULL,
  `sentDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `chat`
--

INSERT INTO `chat` (`messageID`, `userID`, `familyID`, `message`, `sentDate`) VALUES
(1, 2, 0, 'Hello szia', '2018-11-22 12:14:10'),
(2, 2, 2, 'Kaki', '2018-11-22 12:18:39'),
(3, 0, 0, '', '2018-11-22 12:25:50'),
(4, 3, 3, 'Hali', '2019-01-07 10:38:24'),
(5, 3, 3, 'asdasd', '2019-01-07 10:38:36'),
(6, 1, 3, 'Az igen\r\n', '2019-01-14 10:08:41'),
(7, 4, 3, 'Mi az?\r\n', '2019-01-14 10:58:32');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `exercises`
--

CREATE TABLE `exercises` (
  `exerciseID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `exerciseName` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `exerciseCell` varchar(150) COLLATE utf8_hungarian_ci NOT NULL,
  `exerciseDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `exercises`
--

INSERT INTO `exercises` (`exerciseID`, `userID`, `exerciseName`, `exerciseCell`, `exerciseDate`) VALUES
(1, 2, 'Kelés', 'mondayMorning', '2018-11-26 11:44:08'),
(3, 2, 'Ébredés', 'tuesdayMorning', '2018-11-26 11:49:18'),
(5, 2, 'Kimászás az ágyból', 'wednesdayMorning', '2018-11-26 11:50:26'),
(6, 2, 'Ágyazás', 'thursdayMorning', '2018-11-26 11:56:39'),
(7, 2, 'Kajálás', 'wednesdayNoon', '2018-11-26 12:31:43'),
(8, 2, 'Ebédelés', 'thursdayNoon', '2018-11-26 12:31:47'),
(9, 2, 'Zabálás', 'fridayNoon', '2018-11-26 12:31:51'),
(10, 2, 'Alszok', 'wednesdayNight', '2018-11-26 12:32:34'),
(11, 2, 'Alvás', 'tuesdayNight', '2018-11-26 12:32:37'),
(12, 2, 'Csicsika', 'mondayNight', '2018-11-26 12:32:40'),
(13, 2, 'Horkolás', 'thursdayNight', '2018-11-26 12:32:45'),
(14, 2, 'Lóbőrhúzás', 'fridayNight', '2018-11-26 12:32:51'),
(15, 2, 'Döglés', 'saturdayNight', '2018-11-26 12:32:55'),
(16, 2, 'Heverés', 'sundayNight', '2018-11-26 12:33:00'),
(17, 2, 'Suli', 'fridayForenoon', '2018-11-26 12:36:27'),
(18, 1, 'asdasd', 'mondayMorning', '2019-01-11 11:38:58'),
(19, 1, 'fff', 'wednesdayMorning', '2019-01-11 11:39:01'),
(20, 1, 'sada', 'thursdayMorning', '2019-01-11 11:39:02'),
(21, 1, 'asdasd', 'fridayNoon', '2019-01-11 11:39:03'),
(22, 1, 'asdasd', 'fridayLAfterNoon', '2019-01-11 11:39:05'),
(23, 1, 'asdasdasdasdsadasdasd', 'tuesdayMorning', '2019-01-14 10:40:28'),
(24, 1, 'asdasdasdasdasd', 'mondayForenoon', '2019-01-14 10:40:32');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `familys`
--

CREATE TABLE `familys` (
  `familyID` int(11) NOT NULL,
  `familyName` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `familyPicture` text COLLATE utf8_hungarian_ci,
  `adminID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `familys`
--

INSERT INTO `familys` (`familyID`, `familyName`, `familyPicture`, `adminID`) VALUES
(1, 'Nyari', '', 0),
(3, 'Gábor', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `familytouser`
--

CREATE TABLE `familytouser` (
  `familyID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `familytouser`
--

INSERT INTO `familytouser` (`familyID`, `userID`) VALUES
(2, 2),
(3, 2),
(3, 3),
(3, 6),
(3, 7),
(3, 8),
(3, 10),
(3, 4),
(3, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gallery`
--

CREATE TABLE `gallery` (
  `pictureID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `pictureSource` text COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `likes`
--

CREATE TABLE `likes` (
  `programID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `liked` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `likes`
--

INSERT INTO `likes` (`programID`, `userID`, `liked`) VALUES
(2, 4, 1),
(3, 4, 0),
(1, 4, 1),
(4, 4, 0),
(5, 4, 1),
(8, 4, 1),
(8, 3, 0),
(5, 3, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `programs`
--

CREATE TABLE `programs` (
  `programID` int(11) NOT NULL,
  `familyID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `programName` varchar(250) COLLATE utf8_hungarian_ci NOT NULL,
  `programDate` date NOT NULL,
  `programDescription` text COLLATE utf8_hungarian_ci NOT NULL,
  `Likes` int(11) NOT NULL,
  `Dislikes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `programs`
--

INSERT INTO `programs` (`programID`, `familyID`, `userID`, `programName`, `programDate`, `programDescription`, `Likes`, `Dislikes`) VALUES
(1, 3, 1, 'Tesztelés', '2018-11-28', 'A program létrehozás funkciójának tesztelése', 0, 0),
(2, 3, 1, 'Proba', '2018-11-29', 'Proba', 0, 0),
(3, 3, 1, 'Proba2', '2018-01-29', 'Proba2', 0, 0),
(4, 3, 1, 'Mukszik', '2018-11-29', 'Végre', 0, 0),
(5, 3, 1, 'Sajt', '2020-11-29', 'Sajtocska', 0, 0),
(6, 3, 1, 'Csirke', '2020-12-29', 'Csirke', 0, 0),
(7, 3, 1, 'Pulyka', '2018-02-28', 'Pulyka', 0, 0),
(8, 3, 4, 'Vmi', '2019-01-07', 'asd', 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `todolist`
--

CREATE TABLE `todolist` (
  `exerciseID` int(11) NOT NULL,
  `fromUserID` int(11) NOT NULL,
  `toUserID` int(11) NOT NULL,
  `exerciseName` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `exerciseDescription` text COLLATE utf8_hungarian_ci NOT NULL,
  `deadLineDate` date NOT NULL,
  `isDone` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `todolist`
--

INSERT INTO `todolist` (`exerciseID`, `fromUserID`, `toUserID`, `exerciseName`, `exerciseDescription`, `deadLineDate`, `isDone`) VALUES
(1, 3, 2, 'Valami', 'Helokaszioka mioka', '2019-10-04', 1),
(2, 3, 2, 'ASD', 'asd', '2019-11-04', 0),
(3, 3, 1, 'asd', 'asd', '2019-11-04', 1),
(4, 3, 4, 'ASD', 'ASD', '2019-12-04', 1),
(5, 3, 4, 'Proiba feladat', 'Ez egy teszt arra, hogy megnézzük, hogy mi a faszt basztam el már megint.', '2019-01-07', 1),
(6, 3, 4, 'ASD', 'ASDASDASDASD', '2019-01-07', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `First_Name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `Last_Name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `Username` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `Email` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `Position` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
  `profilePicture` text COLLATE utf8_hungarian_ci NOT NULL,
  `Online` tinyint(1) NOT NULL,
  `Points` int(11) DEFAULT '0',
  `Passes` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`Id`, `First_Name`, `Last_Name`, `Username`, `Password`, `Email`, `Position`, `profilePicture`, `Online`, `Points`, `Passes`) VALUES
(1, 'Daniel', 'Gabor', 'GD', '$2y$10$HfoqCu9n8H3Al1q35xnxzewJgnZsFhyjqxQISHTKYAoLNb.ifKzAa', 'gd@gmail.com', 'Bátyj', '../img/profilePictures/gdProf.jpg', 0, 7, 0),
(3, 'Gábor1', 'Zoltán', 'GabZol', '$2y$10$/KByea2vE.1/Oa/FS94R2exbPIfqkkU0YWNcluAVa3VCk0vj1po/e', 'gdborzoltan@gmail.com', 'Apa', '', 1, 0, 0),
(4, 'Nyári', 'Zoltán', 'NyaZol', '$2y$10$HqPVqL7as3qsG3MvCc.Xce7fV.9vYofIdUHHbnxnblJ/yDHRiZYSm', 'zotya19990707@gmail.com', 'Bátyj', '../img/profilePictures/zoliProf.jpg', 0, 3, 0),
(6, 'Gábor', 'Zoltán(ifj)', 'GaborZoltan', '$2y$10$JB6zxD9muejW/WW8r2keA.iSgbdHJwmWDOeeN70/56my3SVNFKu/G', 'ifjgabzol@gmail.com', 'Bátyj', '', 1, 0, 0),
(7, 'Deák', 'Ibolya', 'DeakIbolya', '$2y$10$kI1X3kkzM1KEJOC3py0/DO6kr3jt5dSyFo0RDPzEC6HPhGBVSfwa2', 'deakibolya@gmail.com', 'Anya', '', 1, 0, 0),
(10, 'Valaki1', 'Valami2', 'ValVal', '$2y$10$O34YVICzZ5ACl2SyF5ckmuwDmRGxLAHE5h.PhvA7apT9VUAiC5ZQ6', 'asd@gmail.com', 'Apa', '', 1, 0, 0),
(11, 'Ujj', 'Felhasznalo', 'UjFa', 'asdasdasdasd', 'asdasd@gmail.com', 'Bátyj', '', 0, 0, 0);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`messageID`);

--
-- A tábla indexei `exercises`
--
ALTER TABLE `exercises`
  ADD PRIMARY KEY (`exerciseID`);

--
-- A tábla indexei `familys`
--
ALTER TABLE `familys`
  ADD PRIMARY KEY (`familyID`);

--
-- A tábla indexei `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`pictureID`);

--
-- A tábla indexei `programs`
--
ALTER TABLE `programs`
  ADD PRIMARY KEY (`programID`);

--
-- A tábla indexei `todolist`
--
ALTER TABLE `todolist`
  ADD PRIMARY KEY (`exerciseID`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `chat`
--
ALTER TABLE `chat`
  MODIFY `messageID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT a táblához `exercises`
--
ALTER TABLE `exercises`
  MODIFY `exerciseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT a táblához `familys`
--
ALTER TABLE `familys`
  MODIFY `familyID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT a táblához `gallery`
--
ALTER TABLE `gallery`
  MODIFY `pictureID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT a táblához `programs`
--
ALTER TABLE `programs`
  MODIFY `programID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT a táblához `todolist`
--
ALTER TABLE `todolist`
  MODIFY `exerciseID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
